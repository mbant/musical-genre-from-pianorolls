# Let's re-do the small dataset as a simplification in terms of classes as well as number of examples

import pandas as pd

base_path = '/home/marco/workspace/musical-genre-from-pianorolls/data/'
df = pd.read_csv(base_path+'rock_pop_train_valid.csv')

train = df.loc[ (not valid for valid in df.iloc[:,3] )]
valid = df.loc[ (valid for valid in df.iloc[:,3] )]

small = pd.concat([train.iloc[:1000,:],valid.iloc[:193,:]],axis=0)

small.to_csv(base_path+'small.csv',index=False)
