# Musical Genre Classification from Pianorolls

While this serie of notebooks present a fun (WIP) challenge, *i.e.* Musical Genre classification from Pianorolls, the primary aim is to present a Data Science-y project from start to finish.
Unfortunately the data doesn't seem to support much *mining* on the (granted, quite noisy) subject of genre classification ... I'll keep trying, it's been fun if nothing else to see how the same data can be represented in different ways, each one having its associated benefits and challenges! (and fun overall architecting all the different models associated with every modality!)

The first iteration contains notebooks and generated scripts to train a classifier (using [PyTorch](https://pytorch.org/)) on pianorolls, represented as tensors (think images).

Note that the repo describes the whole process I went through, so you'll notice that doe example the first 3 notebooks are then "deprecated" in favour of a new approach starting from notebook 4.
The idea is to show the complete set of steps I took, to showcase how I approach a new data science idea/project completely from scratch, including first attempts and setbacks.

We then go on trying to convey the MIDI informations via a sequential representation used in [PerformanceRNN](https://magenta.tensorflow.org/performance-rnn) by the Magenta team.

## Dataset and annotations references

### MIDI and Piano Roll Dataset:

[Lakh Pianoroll Dataset](https://salu133445.github.io/lakh-pianoroll-dataset/)

Hao-Wen Dong, Wen-Yi Hsiao, Li-Chia Yang, and Yi-Hsuan Yang, “MuseGAN: Multi-track Sequential Generative Adversarial Networks for Symbolic Music Generation and Accompaniment,” in Proceedings of the 32nd AAAI Conference on Artificial Intelligence (AAAI), 2018

is a derivative of [the Lakh MIDI Dataset v0.1](https://colinraffel.com/projects/lmd)

Colin Raffel. "Learning-Based Methods for Comparing Sequences, with Applications to Audio-to-MIDI Alignment and Matching". PhD Thesis, 2016.

To do a variation on the theme I extract [piano rolls](https://en.wikipedia.org/wiki/Piano_roll) from the MIDIs.


### Annotations:

[Hendrik Schreiber. Improving Genre Annotations for the Million Song Dataset. In Proceedings of the 16th International Society for Music Information Retrieval Conference (ISMIR), pages 241-247, Málaga, Spain, Oct. 2015.](http://www.tagtraum.com/download/schreiber_msdgenre_ismir2015.pdf)

and references therein.
